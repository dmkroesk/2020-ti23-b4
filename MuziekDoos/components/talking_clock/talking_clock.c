
#include "talking_clock.h"
#include <string.h>
#include <time.h>
	
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"

static const char *TAG = "TALKING_CLOCK";


esp_err_t talking_clock_init() {
	
	// Initialize queue
	ESP_LOGI(TAG, "Creating FreeRTOS queue for talking clock");
	talking_clock_queue = xQueueCreate( 10, sizeof( int ) );
	
	if (talking_clock_queue == NULL) {
		ESP_LOGE(TAG, "Error creating queue");
		return ESP_FAIL;
	}
	
	return ESP_OK;
}

esp_err_t talking_clock_fill_queue() {
	
	time_t now;
    struct tm timeinfo;
    time(&now);
	
	char strftime_buf[64];
	localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI(TAG, "The current date/time in Amsterdam is: %s", strftime_buf);
	
	
	// Reset queue
	esp_err_t ret = xQueueReset(talking_clock_queue);
	if (ret != ESP_OK) {
		ESP_LOGE(TAG, "Cannot reset queue");
	}
	
	int data = TALKING_CLOCK_ITSNOW_INDEX;
	
	// Convert hours to AM/PM unit
	int hour = timeinfo.tm_hour;
	hour = (hour == 0 ? 12 : hour);
	hour = (hour > 12 ? hour%12 : hour);
	data = TALKING_CLOCK_1_INDEX-1+hour;
	ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);

	data = TALKING_CLOCK_HOUR_INDEX;
	ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);

	int minute = timeinfo.tm_min;
	int first = minute;
	int second = minute % 10;
	while(first >= 10)
	{
		first = first / 10;
	}
	first *= 10;

	printf("Minute:%i\t First:%i \t Second:%i\n",minute,first,second);

	

	if (minute > 14) { 					
		if (second == 0 ) {		// 20,30,40,50
			data = minute/10 + 15;
			ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		} else if(minute < 20) {		//15-19
			//Eerste digit
			data = second + 2;
			ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY); 
			//Tien
			data = 12;
			ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY); 
		} else {						//De rest	
			//Eerste digit
			data = second + 2;
			ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY); 
			//AND
			data = TALKING_CLOCK_AND_INDEX;
			printf("AND");
			ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
			//Tiental
			data = minute/10 + 15;
			printf("Minute:%i",data);
			ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		}
	} else if (minute != 0) {							//1-14
		data = minute + 2;
		printf("Minute:%i",data);
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	}
	
	ESP_LOGI(TAG, "Queue filled with %d items", uxQueueMessagesWaiting(talking_clock_queue));
	
	return ESP_OK;
}


